package com.example.utilisateursapi;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.app.UiModeManager.MODE_NIGHT_YES;

/**
 * An activity representing a list of Items. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ItemDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ItemListActivity extends AppCompatActivity {
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private int currentPage = 1;
    private String countPerPage = "15";
    private int lastPage;
    private final String BASE_URL = "https://vuetable.ratiw.net/api/users";
    private String url = BASE_URL;
    boolean isDark;

    private TextView pageIndicator;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        isDark = getSharedPreferences("THEME", Context.MODE_PRIVATE).getBoolean("DarkTheme", false);
        if (isDark) {
            setTheme(R.style.DarkTheme_NoActionBar);
        } else {
            setTheme(R.style.AppTheme_NoActionBar);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        pageIndicator = (TextView) findViewById(R.id.page_count);
        setupPaginationButtons();
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        if (findViewById(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        recyclerView = (RecyclerView) findViewById(R.id.item_list);
        getUsersList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list_menu, menu);
        SharedPreferences pref = getSharedPreferences("THEME", Context.MODE_PRIVATE);
        menu.findItem(R.id.action_theme).setIcon(isDark ? R.drawable.ic_format_color_fill_white_24dp : R.drawable.ic_format_color_fill_black_24dp);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Choose an option")
                        .setItems(R.array.pagecount_array, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                 String[] options = getResources().getStringArray(R.array.pagecount_array);
                                 countPerPage = options[which];
                                 changePage(currentPage);
                                 dialog.dismiss();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                break;
            case R.id.action_theme:
                SharedPreferences pref = getSharedPreferences("THEME", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean("DarkTheme", !isDark);
                editor.commit();
                closeOptionsMenu();
                recreate();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView, List<User> users) {
        recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(this, users, mTwoPane));
    }

    private void getUsersList() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            currentPage = response.getInt("current_page");
                            lastPage = response.getInt("last_page");
                            pageIndicator.setText(currentPage + " / " + lastPage);
                            SimpleItemRecyclerViewAdapter adapter = (SimpleItemRecyclerViewAdapter) recyclerView.getAdapter();
                            List<User> users = generateUserList(response.getJSONArray("data"));
                            if (adapter == null) {
                                setupRecyclerView(recyclerView, users);
                            } else {
                                adapter.setValues(generateUserList(response.getJSONArray("data")));
                                adapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error

                    }
                });

        ApiHandler.getInstance(this).addToRequestQueue(jsonObjectRequest);

    }

    private List<User> generateUserList(JSONArray json) throws JSONException {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < json.length(); i++) {
            users.add(new User(json.getJSONObject(i)));
        }
        return users;
    }

    private void setupPaginationButtons() {
        final ImageButton firstPageButton = (ImageButton) findViewById(R.id.first_page);
        final ImageButton previousPageButton = (ImageButton) findViewById(R.id.previous_page);
        final ImageButton nextPageButton = (ImageButton) findViewById(R.id.next_page);
        final ImageButton lastPageButton = (ImageButton) findViewById(R.id.last_page);

        firstPageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePage(1);
                previousPageButton.setClickable(false);
                firstPageButton.setClickable(false);
                nextPageButton.setClickable(true);
                lastPageButton.setClickable(true);
            }
        });
        previousPageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentPage > 1) {
                    if (currentPage == 2) {
                        previousPageButton.setClickable(false);
                        firstPageButton.setClickable(false);
                    }
                    changePage(currentPage - 1);
                    nextPageButton.setClickable(true);
                    lastPageButton.setClickable(true);
                }
            }
        });
        nextPageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentPage < lastPage) {
                    if (currentPage == lastPage - 1) {
                        nextPageButton.setClickable(false);
                        lastPageButton.setClickable(false);
                    }
                    changePage(currentPage + 1);
                    previousPageButton.setClickable(true);
                    firstPageButton.setClickable(true);
                }
            }
        });
        lastPageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePage(lastPage);
                previousPageButton.setClickable(true);
                firstPageButton.setClickable(true);
                nextPageButton.setClickable(false);
                lastPageButton.setClickable(false);
            }
        });
    }

    private void changePage(int page) {
        url = BASE_URL + "?page=" + page + "&per_page=" + countPerPage;
        getUsersList();
    }

    public static class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final ItemListActivity mParentActivity;
        private List<User> mValues;
        private final boolean mTwoPane;
        private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User item = (User) view.getTag();
                if (mTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putSerializable("user", item);
                    ItemDetailFragment fragment = new ItemDetailFragment();
                    fragment.setArguments(arguments);
                    mParentActivity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.item_detail_container, fragment)
                            .commit();
                } else {
                    Context context = view.getContext();
                    Intent intent = new Intent(context, ItemDetailActivity.class);
                    intent.putExtra("user", item);
                    context.startActivity(intent);
                }
            }
        };

        SimpleItemRecyclerViewAdapter(ItemListActivity parent,
                                      List<User> items,
                                      boolean twoPane) {
            mValues = items;
            mParentActivity = parent;
            mTwoPane = twoPane;
        }

        public void setValues(List<User> values) {
            mValues = values;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mIdView.setText(String.valueOf(mValues.get(position).getId()));
            holder.mContentView.setText(mValues.get(position).getName());

            holder.itemView.setTag(mValues.get(position));
            holder.itemView.setOnClickListener(mOnClickListener);
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final TextView mIdView;
            final TextView mContentView;

            ViewHolder(View view) {
                super(view);
                mIdView = (TextView) view.findViewById(R.id.id_text);
                mContentView = (TextView) view.findViewById(R.id.content);
            }
        }
    }
}
