package com.example.utilisateursapi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class User implements Serializable {
    private int id;
    private String name;
    private String email;
    private String nickname;
    private String birthDate;
    private String gender;
    private double salary;
    private int age;
    private String phoneNumber;
    private String address1;
    private String address2;
    private String zipcode;

    public User(JSONObject obj) {
        try {
            id = obj.getInt("id");
            name = obj.getString("name");
            nickname = obj.getString("nickname");
            email = obj.getString("email");
            birthDate = obj.getString("birthdate").split(" ")[0];
            gender = obj.getString("gender");
            salary = Float.valueOf(obj.getString("salary"));
            age = obj.getInt("age");
            JSONObject address = obj.getJSONObject("address");
            phoneNumber = address.getString("mobile");
            address1 = address.getString("line1");
            address2 = address.getString("line2");
            zipcode = address.getString("zipcode");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getGender() {
        return gender;
    }

    public double getSalary() {
        return salary;
    }

    public int getAge() {
        return age;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public String getZipcode() {
        return zipcode;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getNickname() {
        return nickname;
    }

    public String getBirthDate() {
        return birthDate;
    }
}
