package com.example.utilisateursapi;

import android.os.Bundle;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends Fragment {
    /**
     * The user this fragment is presenting.
     */
    private User mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        for (String key : getArguments().keySet()) {
            Log.d("KEY", key);
        }
        if (getArguments().containsKey("user")) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = (User) getArguments().getSerializable("user");

            FragmentActivity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.getName());
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);

        // Show the user as text in a TextView.
        StringBuilder sb = new StringBuilder();
        if (mItem != null) {
            sb.append("Id : " + mItem.getId() + '\n');
            sb.append("Nom : " + mItem.getName() + '\n');
            sb.append("Surnom " + mItem.getNickname() + '\n');
            sb.append("e-mail : " + mItem.getEmail() + '\n');
            sb.append("Date de naissance : " + mItem.getBirthDate() + '\n');
            sb.append("Sexe : " + mItem.getGender() + '\n');
            sb.append("Salaire : " + mItem.getSalary() + '\n');
            sb.append("Age : " + mItem.getAge() + '\n');
            sb.append("Adresse 1 : " + mItem.getAddress1() + '\n');
            sb.append("Adresse 2 : " + mItem.getAddress2() + '\n');
            sb.append("Code postal : " + mItem.getZipcode() + '\n');
            sb.append("Mobile : " + mItem.getPhoneNumber());
            ((TextView) rootView.findViewById(R.id.item_detail)).setText(sb.toString());
        }

        FragmentActivity activity = this.getActivity();
        CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
        if (appBarLayout == null) {
            Bundle arguments = new Bundle();
            arguments.putSerializable("user", mItem);
            ActionButtonsTablet fragment = new ActionButtonsTablet();
            fragment.setArguments(arguments);
            activity.getSupportFragmentManager().beginTransaction()
                    .add(R.id.action_buttons_container, fragment)
                    .commit();
        }

        return rootView;
    }
}
