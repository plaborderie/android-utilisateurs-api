package com.example.utilisateursapi;


import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


/**
 * A simple {@link Fragment} subclass.
 */
public class ActionButtonsTablet extends Fragment {

    private User user;

    public ActionButtonsTablet() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey("user")) {
            user = (User) getArguments().getSerializable("user");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_action_buttons_tablet, container, false);
        FloatingActionButton message = (FloatingActionButton) rootView.findViewById(R.id.button_message);
        FloatingActionButton mail = (FloatingActionButton) rootView.findViewById(R.id.button_mail);
        final Activity activity = this.getActivity();

        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                assert user != null;
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:" + user.getPhoneNumber()));
                sendIntent.putExtra("sms_body", "Dear " + user.getName());
                startActivity(sendIntent);
            }
        });
        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assert user != null;
                String content = "Hi " + user.getName();
                ApplicationInfo appinfo = activity.getApplicationInfo();
                int stringId = appinfo.labelRes;
                String appName = stringId == 0 ? appinfo.nonLocalizedLabel.toString() : activity.getString(stringId);
                String subject = appName + " information";

                Intent email = new Intent();
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{ user.getEmail() });
                email.putExtra(Intent.EXTRA_SUBJECT, subject);
                email.putExtra(Intent.EXTRA_TEXT, content);
                email.setType("sms/rfc822");

                startActivity(Intent.createChooser(email, "Choose an email client"));
            }
        });

        return rootView;
    }

}
